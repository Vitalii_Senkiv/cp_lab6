package lab.six;

import lab.six.Services.EmailNotifier;
import lab.six.Services.LibraryService;
import lab.six.UI.ConsoleUi;

import java.time.LocalDate;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            var ui = new ConsoleUi(scanner);
            ui.start();
        }
    }
}