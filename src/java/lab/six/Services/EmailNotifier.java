package lab.six.Services;

import lab.six.DTOs.BookDTO;
import lab.six.DTOs.ReaderPassDTO;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

public class EmailNotifier {
    public void notify(List<String> emails, String message) {
        emails.stream()
                .forEach(email -> System.out.println("Hello " + email + message));
    }
    public void notifyAboutLibraryNews(List<String> emails) {
        notify(emails, "! We have great news! Visit our library and you can...");
    }
    public void notifyUsersAboutDeadlines(List<String> emails, LibraryService library, LocalDate sendingDate) {
        var records = library.getBookBorrowingRecords();
        var readers = library.getReaders();
        var books = library.getBooks();
        emails.stream()
              .forEach(email -> {
                  var outOfDateRecordsStream = records.stream()
                          .filter(record -> record.getReaderId() == readers.stream()
                                 .filter(reader -> reader.getEmail().equals(email))
                                 .findAny().orElse(ReaderPassDTO.getNullRecord()).getId())
                          .filter(record -> !record.equals(ReaderPassDTO.getNullRecord()))
                          .filter(record -> sendingDate.compareTo(record.getEndDate()) >= 0)
                          .filter(record -> record.getActualEndDate().compareTo(sendingDate) > 0);

                  var booksToReturnMap = outOfDateRecordsStream
                          .collect(Collectors.toMap(record -> books.stream()
                                      .filter(book -> book.getId() == record.getBookId())
                                      .findAny().orElse(BookDTO.getNullBook()),
                                  record -> record));

                  if (booksToReturnMap.size() > 0) {
                      System.out.println("Dear " + email + " we want to remember you" +
                              " that you should return these books:");

                      booksToReturnMap.forEach((book, record) ->
                              System.out.println(book.getAuthor() + ":" + book.getName()
                                      + "\nExpected return date: " + record.getEndDate()
                                      + "\nSending date: " + sendingDate
                                      + "\nOverdue days: " + ChronoUnit.DAYS.between(record.getEndDate(),
                                      sendingDate)));
                  }
              });
    }
}
