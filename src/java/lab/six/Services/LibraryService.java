package lab.six.Services;

import lab.six.DTOs.BookBorrowingRecordDTO;
import lab.six.DTOs.BookDTO;
import lab.six.DTOs.ReaderPassDTO;
import lab.six.Parsers.JsonBookParser;
import lab.six.Parsers.LibrarySerializer;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class LibraryService {
    private final ArrayList<BookDTO> books;
    private final ArrayList<ReaderPassDTO> readers;
    private final ArrayList<BookBorrowingRecordDTO> bookBorrowingRecords;

    public ArrayList<BookDTO> getBooks() {
        return books;
    }
    public ArrayList<ReaderPassDTO> getReaders() {
        return readers;
    }
    public ArrayList<BookBorrowingRecordDTO> getBookBorrowingRecords() {
        return bookBorrowingRecords;
    }
    public LibraryService() {
        var parser = new JsonBookParser();
        books = parser.getBooks("resources/Books.json");
        readers = parser.getReaders("resources/Readers.json");
        bookBorrowingRecords = parser.getBorrowingRecords("resources/BookBorrowingRecords.json");
    }

    public List<BookDTO> getBooksSortedByPublicationYear() {
        return books.stream()
                .sorted((b1, b2) -> b1.getPublicationYear().getYear() - b2.getPublicationYear().getYear())
                .collect(Collectors.toList());
    }

    public List<String> getEmailsByBooksCountCondition(Function<Long, Boolean> condition) {

        return bookBorrowingRecords.stream()
                .map(record -> record.getReaderId())
                .filter(id -> condition.apply(bookBorrowingRecords.stream()
                            .filter(record -> record.getReaderId() == id)
                            .count()))
                .distinct()
                .map(id -> readers.stream()
                        .filter(reader -> reader.getId() == id)
                        .findAny().orElse(ReaderPassDTO.getNullRecord()).getEmail())
                .collect(Collectors.toList());
    }

    public long countReadersByAuthor(String authorName) {
        List<Integer> bookIds = books.stream()
                .filter(book -> book.getAuthor().equals(authorName))
                .map(book -> book.getId()).collect(Collectors.toList());

        return bookBorrowingRecords.stream()
                .filter(record -> bookIds.contains(record.getBookId()))
                .map(record -> record.getReaderId())
                .distinct()
                .count();
    }

    public long findMaxBorrowedBooksCount() {
        return bookBorrowingRecords.stream()
                .map(record -> record.getReaderId())
                .distinct()
                .mapToLong(readerId -> bookBorrowingRecords.stream()
                        .filter(record -> record.getReaderId() == readerId)
                        .count())
                .max().orElse(0);
    }
    public void serializeBooks() {
        new LibrarySerializer().serializeBooks(this);
    }

    public List<BookDTO> deserializeBooks() {
        return new LibrarySerializer().deserializeBooks();
    }
}
