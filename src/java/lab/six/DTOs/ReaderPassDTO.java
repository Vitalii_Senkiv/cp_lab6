package lab.six.DTOs;

import java.util.ArrayList;

public class ReaderPassDTO {
    private int id;
    private String readerName;
    private String readerSurname;
    private String readerPatronymic;
    private String email;

    public ReaderPassDTO(int id, String readerName, String readerSurname, String readerPatronymic,
                         String email) {
        this.id = id;
        this.readerName = readerName;
        this.readerSurname = readerSurname;
        this.readerPatronymic = readerPatronymic;
        this.email = email;
    }

    static public ReaderPassDTO getNullRecord() {
        return new ReaderPassDTO(0, "none", "none", "none", "none");
    }
    public String getEmail() {
        return email;
    }

    public String getReaderPatronymic() {
        return readerPatronymic;
    }

    public String getReaderSurname() {
        return readerSurname;
    }

    public String getReaderName() {
        return readerName;
    }

    public int getId() {
        return id;
    }
}
