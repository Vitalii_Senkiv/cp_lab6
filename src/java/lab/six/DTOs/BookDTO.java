package lab.six.DTOs;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

public class BookDTO implements Serializable {
    private int id;
    private String author;
    private String name;
    private LocalDate publicationYear;

    public static BookDTO getNullBook() {
        return new BookDTO(0, "none", "none", LocalDate.now());
    }
    public BookDTO(int id, String author, String name, LocalDate publicationYear) {
        this.id = id;
        this.author = author;
        this.name = name;
        this.publicationYear = publicationYear;
    }

    public String getAuthor() {
        return author;
    }

    public String getName() {
        return name;
    }

    public LocalDate getPublicationYear() {
        return publicationYear;
    }

    public int getId() {
        return id;
    }
}
