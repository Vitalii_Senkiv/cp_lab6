package lab.six.DTOs;

import java.time.LocalDate;
import java.util.Date;

public class BookBorrowingRecordDTO {
    private LocalDate startDate;
    private LocalDate endDate;
    private LocalDate actualEndDate;

    private int readerId;
    private int bookId;


    public BookBorrowingRecordDTO(LocalDate startDate, LocalDate endDate, LocalDate endReal, int readerId, int bookId) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.actualEndDate = endReal;
        this.readerId = readerId;
        this.bookId = bookId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public LocalDate getActualEndDate() {
        return actualEndDate;
    }

    public int getReaderId() {
        return readerId;
    }

    public int getBookId() {
        return bookId;
    }
}
