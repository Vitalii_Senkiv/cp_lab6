package lab.six.Parsers;

import lab.six.DTOs.BookBorrowingRecordDTO;
import lab.six.DTOs.BookDTO;
import lab.six.DTOs.ReaderPassDTO;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.function.Function;

public class JsonBookParser {
    private static final String NAME_TAG = "name";
    private static final String AUTHOR_TAG = "author";
    private static final String PUBLICATION_YEAR_TAG = "year";
    private static final String ID_TAG = "id";
    private static final String READER_ID_TAG = "id";
    private static final String READER_NAME_TAG = "name";
    private static final String READER_SURNAME_TAG = "surname";
    private static final String READER_PATRONYMIC_TAG = "patronymic";
    private static final String READER_EMAIL_TAG = "email";

    private static final String RECORD_BORROWING_START_DATE_TAG = "start_date";
    private static final String RECORD_BORROWING_END_DATE_TAG = "end_date";
    private static final String RECORD_BORROWING_ACTUAL_END_DATE_TAG = "actual_end_date";
    private static final String RECORD_READER_ID_TAG = "reader_id";
    private static final String RECORD_BOOK_ID_TAG = "book_id";

    private JSONParser parser = new JSONParser();

    public <T> ArrayList<T> getArray(String fileName, Function<JSONObject, T> converterMethod) {
        var objects = new ArrayList<T>();

        try (FileReader reader = new FileReader(fileName)) {
            JSONArray jsonArray = (JSONArray) parser.parse(reader);

            for (Object jsonObject : jsonArray) {
                var object = converterMethod.apply((JSONObject) jsonObject);
                objects.add(object);
            }
        } catch (Exception e) {

            System.out.println("Parsing error" + e.toString());
        }

        return objects;
    }
    public ArrayList<BookDTO> getBooks(String fileName) {
        return getArray(fileName, jsonObject -> convertJsonObjectToBookDTO(jsonObject));
    }

    public ArrayList<ReaderPassDTO> getReaders(String fileName) {
        return getArray(fileName, jsonObject -> convertJsonObjectToReaderPassDTO(jsonObject));
    }

    public ArrayList<BookBorrowingRecordDTO> getBorrowingRecords(String fileName) {
        return getArray(fileName, jsonObject -> convertJsonObjectToBookBorrowingRecordDTO(jsonObject));
    }

    private BookDTO convertJsonObjectToBookDTO(JSONObject jsonObject) {
        int id = (int)(long) jsonObject.get(ID_TAG);
        String author = (String) jsonObject.get(AUTHOR_TAG);
        String name = (String) jsonObject.get(NAME_TAG);
        int year = (int)(long) jsonObject.get(PUBLICATION_YEAR_TAG);

        return new BookDTO(id, author, name, LocalDate.of(year, 1, 1));
    }

    private ReaderPassDTO convertJsonObjectToReaderPassDTO(JSONObject jsonObject) {
        int id = (int)(long) jsonObject.get(READER_ID_TAG);
        String name = (String) jsonObject.get(READER_NAME_TAG);
        String surname = (String) jsonObject.get(READER_SURNAME_TAG);
        String patronymic = (String) jsonObject.get(READER_PATRONYMIC_TAG);
        String email = (String) jsonObject.get(READER_EMAIL_TAG);

        return new ReaderPassDTO(id, name, surname, patronymic, email);
    }

    private BookBorrowingRecordDTO convertJsonObjectToBookBorrowingRecordDTO(JSONObject jsonObject) {
        var startDate = LocalDate.parse((String)jsonObject.get(RECORD_BORROWING_START_DATE_TAG));
        var endDate = LocalDate.parse((String)jsonObject.get(RECORD_BORROWING_END_DATE_TAG));
        var realEndDate = LocalDate.parse((String)jsonObject.get(RECORD_BORROWING_ACTUAL_END_DATE_TAG));

        int readerId = (int)(long) jsonObject.get(RECORD_READER_ID_TAG);
        int bookId = (int)(long) jsonObject.get(RECORD_BOOK_ID_TAG);

        return new BookBorrowingRecordDTO(startDate, endDate, realEndDate, readerId, bookId);
    }
}
