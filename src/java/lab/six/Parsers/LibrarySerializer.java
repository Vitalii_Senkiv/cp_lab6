package lab.six.Parsers;

import lab.six.DTOs.BookDTO;
import lab.six.Services.LibraryService;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class LibrarySerializer {
    public void serializeBooks(LibraryService library) {
        try
        {
            FileOutputStream fos = new FileOutputStream("resources/SerializedBooks.txt");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(library.getBooks());
            oos.close();
            fos.close();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
    }
    public List<BookDTO> deserializeBooks() {
        var books = new ArrayList<BookDTO>();
        try
        {
            FileInputStream fis = new FileInputStream("resources/SerializedBooks.txt");
            ObjectInputStream ois = new ObjectInputStream(fis);

            books = (ArrayList<BookDTO>) ois.readObject();

            ois.close();
            fis.close();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
            return null;
        }
        catch (ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
            return null;
        }

        return books;
    }
}
