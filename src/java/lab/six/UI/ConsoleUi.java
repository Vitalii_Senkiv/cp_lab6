package lab.six.UI;

import lab.six.DTOs.BookDTO;
import lab.six.Services.EmailNotifier;
import lab.six.Services.LibraryService;

import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

public class ConsoleUi {
    private LibraryService service;
    private List<BookDTO> sorted;
    private List<String> activeReadersEmails;
    private List<String> passiveReadersEmails;
    private List<String> allEmails;
    private long maxBorrowedBooksCount;
    private EmailNotifier notifier = new EmailNotifier();
    private int taskCount = 6;
    private Scanner scanner;

    public ConsoleUi(Scanner scanner) {
        this.scanner = scanner;

        service = new LibraryService();

        service.serializeBooks();
        var books = service.deserializeBooks();

        sorted = service.getBooksSortedByPublicationYear();

        activeReadersEmails = service.getEmailsByBooksCountCondition(count -> count > 1);

        passiveReadersEmails = service.getEmailsByBooksCountCondition(count -> count <= 1);

        allEmails = service.getEmailsByBooksCountCondition(count -> true);

        maxBorrowedBooksCount = service.findMaxBorrowedBooksCount();
    }
    private void showTaskResult(int num) {
        switch (num) {
            case 1:
                System.out.println("Sorted books by publication year:");
                sorted.forEach(book -> System.out.println("(" + book.getPublicationYear() + ")["
                        + book.getAuthor() + "] " + book.getName()));
                break;
            case 2:
                System.out.println("These persons took more than 1 book:");
                activeReadersEmails.forEach(System.out::println);
                System.out.println("These persons took 1 or less books:");
                passiveReadersEmails.forEach(System.out::println);
                break;
            case 3:
                System.out.println("Please, enter author name:");

                String author = scanner.nextLine();
                var readersCount = service.countReadersByAuthor(author);
                System.out.println("There are " + readersCount + " reader" + (readersCount == 1 ? "" : "s"));
                break;
            case 4:
                System.out.println("Max borrowed by one reader books count: " + maxBorrowedBooksCount);
                break;
            case 5:
                notifier.notifyAboutLibraryNews(passiveReadersEmails);
                //notifier.notifyUsersAboutDeadlines(activeReadersEmails, service, LocalDate.now());
                notifier.notifyUsersAboutDeadlines(activeReadersEmails, service,
                        LocalDate.of(2022, 2, 10));
                break;
            case 6:
                notifier.notifyUsersAboutDeadlines(allEmails, service, LocalDate.now());
                break;
        }
    }
    public void start() {
        while (true) {
            while (true) {
                System.out.println("Please, choose task num(from 1 to " + taskCount + " or 0 to exit): ");
                var num = scanner.nextInt();
                scanner.nextLine();//to remove \n from stream

                if (num == 0)
                    return;
                if (num > 0 && num <= taskCount) {
                    showTaskResult(num);
                    break;
                }

                System.out.println("Sorry, num is in incorrect range:(");
            }

        }
    }
}
